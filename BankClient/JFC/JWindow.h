#ifndef _JWINDOW_H_
#define _JWINDOW_H_

#include "JWindowBase.h"
#include "JApplication.h"
#include <vector>

namespace jfc {

class JEvent;

class JWindow : public JWindowBase
{
public:
	JWindow();
	JWindow(SHORT x, SHORT y, SHORT width, SHORT height,
			JWindow *parent = jApp->Root());
	virtual ~JWindow();

	//增加子窗口
	void AddChild(JWindow *child);
	//删除子窗口
	void DelChild(JWindow *child);

	virtual void OnKeyEvent(JEvent *e);
	virtual void Draw();
	void Show();

	//判断当前窗口是否为应用的激活窗口 
	bool IsCurrent() { return this == jApp->GetCurrent(); }
	//设置当前窗口为应用的窗口
	void SetCurrent();


protected:
	JWindow *parent_;
	//用于存放子窗口
	std::vector<JWindow *> children_;
};

}	//namespace jfc

#endif	//_JWINDOW_H_

