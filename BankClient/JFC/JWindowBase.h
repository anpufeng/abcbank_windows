#ifndef _JWINDOW_BASE_H_
#define  _JWINDOW_BASE_H_

#include <windows.h>
#include <string>
#include <iostream>

namespace jfc {

	// 当color = 0	表示 黑色
	// 当color = 1	表示 蓝色
	// 当color = 2	表示 绿色
	// 当color = 3	表示 青色
	// 当color = 4	表示 红色
	// 当color = 5	表示 洋红
	// 当color = 6	表示 棕色
	// 当color = 7	表示 淡灰
	// 当color = 8	表示 深灰
	// 当color = 9	表示 淡蓝
	// 当color = 10 表示 淡绿
	// 当color = 11 表示 淡青
	// 当color = 12 表示 淡红
	// 当color = 13 表示 淡洋红
	// 当color = 14 表示 黄色
	// 当color = 15 表示 白色

#define FCOLOR_BLACK			0
#define FCOLOR_BLUE				FOREGROUND_BLUE
#define FCOLOR_GREEN			FOREGROUND_GREEN
#define FCOLOR_CYAN				FOREGROUND_BLUE | FOREGROUND_GREEN
#define FCOLOR_RED				FOREGROUND_RED
#define FCOLOR_MAGENTA			FOREGROUND_RED | FOREGROUND_BLUE
#define FCOLOR_BLOWN			FOREGROUND_RED | FOREGROUND_GREEN
#define FCOLOR_GRAY				FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE
#define FCOLOR_DARKGRAY			FCOLOR_BLACK + FOREGROUND_INTENSITY
#define FCOLOR_LIGHTBLUE		FCOLOR_BLUE + FOREGROUND_INTENSITY
#define FCOLOR_LIGHTGREEN		FCOLOR_GREEN + FOREGROUND_INTENSITY
#define FCOLOR_LIGHTCYAN		FCOLOR_CYAN + FOREGROUND_INTENSITY
#define FCOLOR_LIGHTRED			FCOLOR_RED + FOREGROUND_INTENSITY
#define FCOLOR_LIGHTMAGENTA		FCOLOR_MAGENTA + FOREGROUND_INTENSITY
#define FCOLOR_YELLO			FCOLOR_BLOWN + FOREGROUND_INTENSITY
#define FCOLOR_WHITE			FCOLOR_GRAY + FOREGROUND_INTENSITY

#define BCOLOR_BLACK			0
#define BCOLOR_BLUE				BACKGROUND_BLUE
#define BCOLOR_GREEN			BACKGROUND_GREEN
#define BCOLOR_CYAN				BACKGROUND_BLUE | BACKGROUND_GREEN
#define BCOLOR_RED				BACKGROUND_RED
#define BCOLOR_MAGENTA			BACKGROUND_RED | BACKGROUND_BLUE
#define BCOLOR_BLOWN			BACKGROUND_RED | BACKGROUND_GREEN
#define BCOLOR_GRAY				BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE
#define BCOLOR_DARKGRAY			BCOLOR_BLACK + BACKGROUND_INTENSITY
#define BCOLOR_LIGHTBLUE		BCOLOR_BLUE + BACKGROUND_INTENSITY
#define BCOLOR_LIGHTGREEN		BCOLOR_GREEN + BACKGROUND_INTENSITY
#define BCOLOR_LIGHTCYAN		BCOLOR_CYAN + BACKGROUND_INTENSITY
#define BCOLOR_LIGHTRED			BCOLOR_RED + BACKGROUND_INTENSITY
#define BCOLOR_LIGHTMAGENTA		BCOLOR_MAGENTA + BACKGROUND_INTENSITY
#define BCOLOR_YELLO			BCOLOR_BLOWN + BACKGROUND_INTENSITY
#define BCOLOR_WHITE			BCOLOR_GRAY + BACKGROUND_INTENSITY

#define KEY_ENTER 13
#define KEY_ESC 27
#define KEY_(n) (48+n)
#define KEY_UP ((224<<8) + 72)			// 57416
#define KEY_DOWN ((224<<8) + 80)		// 57424
#define KEY_LEFT ((224<<8) + 75)		// 57419
#define KEY_RIGHT ((224<<8) + 77)		// 57421
#define KEY_DEL ((224<<8) + 83)			// 57427
#define KEY_BACK 8
#define KEY_TAB 9
#define KEY_HOME ((224<<8) + 71)		// 57415
#define KEY_PGUP ((224<<8) + 73)		// 57417
#define KEY_END ((224<<8) + 79)			// 57423
#define KEY_PGDN ((224<<8) + 81)		// 57425
#define KEY_F11 ((224<<8) + 133)		// 57477
#define KEY_F12 ((224<<8) + 134)		// 57478
#define KEY_F(n) (-58-n)				// 57403~57412
#define KEY_INS ((224<<8) + 82)			// 57425

#define COLUMN_COUNT		80
#define ROW_COUNT			25

typedef SMALL_RECT JRECT;

class JWindowBase
{
public:
	JWindowBase();
	JWindowBase(SHORT x, SHORT y, SHORT width, SHORT height);
	virtual ~JWindowBase();
	SHORT Width() const { return width_; }
	SHORT Height() const { return height_; }
	void SetTextColor(WORD color) { fColor_ = color; }
	WORD GetTextColor () const { return fColor_; }
	void SetBkColor(WORD color) { bColor_ = color; }
	WORD GetBkColor() const { return bColor_; }

	//设置光标位置
	void SetCursorPos(SHORT x, SHORT y);
	//刷新屏幕
	void Refresh();
	//绘制矩形区域)
	void FillRect(JRECT rect);
	//绘制文本(x,y相对于本窗口的位置,而非整个窗口)
	void DrawText(SHORT x, SHORT y, const std::string &text);
	//在窗口中间绘制文本
	void DrawText(const std::string &text);
	//水平位置画线
	void DrawHLine(SHORT y, SHORT x1, SHORT x2, char ch);
	//垂直位置画线
	void DrawVLine(SHORT x, SHORT y1, SHORT y2, char ch);
	//清除窗口
	void ClearWindow();
private:
	class ScreenBuffer {
	public:
		ScreenBuffer() {}
		~ScreenBuffer() {}
		void Write(SHORT x, SHORT y, CHAR_INFO ci) {
			buffer_[COLUMN_COUNT * y + x] = ci;
		}

		void Write(SHORT x, SHORT y, const std::string &str, WORD fColor, WORD bColor) {
			CHAR_INFO ci;
			ci.Attributes = fColor | bColor;
			
			for (size_t i = 0; i < str.length(); ++i) {
				ci.Char.AsciiChar = str[i];
				Write(x, y, ci);
			}
		}

		void Refresh(SHORT x, SHORT y, SHORT width, SHORT height) {
			COORD size = {COLUMN_COUNT, ROW_COUNT};
			COORD cood = {x, y};
			JRECT rect = {x, y, x+width-1, y+height-1};
			WriteConsoleOutput(GetStdHandle(STD_OUTPUT_HANDLE), buffer_, size, cood, &rect);
		}
		CHAR_INFO buffer_[COLUMN_COUNT * ROW_COUNT];

	};

public:
	static ScreenBuffer sb_;
protected:
	SHORT x_;
	SHORT y_;
	SHORT width_;
	SHORT height_;
	//前景色(文本颜色)
	WORD fColor_;
	//背景色
	WORD bColor_;
};

}	//namespace jfc
#endif	//_JWINDOW_BASE_H_
