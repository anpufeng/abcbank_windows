#ifndef _JEVENT_H_
#define _JEVENT_H_

namespace jfc {

class JWindow;

class JEvent
{
public:
	JEvent(int code, JWindow *sender) :
	  code_(code), sender_(sender), done_(false) {

	}
	~JEvent();

	JWindow *GetSender() const { return sender_; }
	int GetCode() const { return code_; }
	void Done() { done_ = true; }
	bool IsDone() const { return done_ == true; };

private:
	int code_;
	JWindow *sender_;
	bool done_;
};


}	//namespace jfc

#endif	//_JEVENT_H_

