#ifndef _JAPPLICATION_H_
#define _JAPPLICATION_H_

namespace jfc {

class JWindow;

//特殊的单例模式, 模仿自QT
class JApplication
{
public:
	JApplication();
	~JApplication();

	static JApplication *Instance() { return self_; }
	JWindow *Root() const { return root_; }
	JWindow *GetCurrent() const { return current_; }
	void SetCurrent(JWindow *win) { current_ = win; }
	//消息循环
	int Exec();
	//显示光标
	void ShowCursor() const;
	void HideCursor() const;

private:

	static JApplication *self_;
	//当前接收按键的窗口
	JWindow *current_;
	//根窗口
	JWindow *root_;
};

#define  jApp		JApplication::Instance()

}	//namespace jfc

#endif	//_JAPPLICATION_H_