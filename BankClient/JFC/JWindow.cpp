#include "JWindow.h"
#include "JEvent.h"
#include "JApplication.h"

#include <algorithm>

namespace jfc {


JWindow::JWindow() : parent_(NULL)
{
}

JWindow::JWindow(SHORT x, SHORT y, SHORT width, SHORT height, JWindow *parent) 
	: JWindowBase(x, y, width, height), parent_(parent) {
	if (parent_) {
		parent_->AddChild(this);
	}
}
JWindow::~JWindow()
{
	if (parent_) {
		parent_->DelChild(this);
	}
}

void JWindow::OnKeyEvent(JEvent *e){

}

void JWindow::Draw() {
	SetTextColor(FCOLOR_BLACK);
	SetBkColor(BCOLOR_WHITE);
	JRECT rect = {0, 0, Width() - 1, Height() -1};
	FillRect(rect);
}

void JWindow::Show() {
	SetCurrent();	//设置当前窗口
	Draw();			//绘制, 仅仅是绘制到缓冲区
	Refresh();		//真正的绘制
}

void JWindow::AddChild(JWindow *child) {
	children_.push_back(child);
}

void JWindow::DelChild(JWindow *child) {
	children_.erase(std::remove(children_.begin(), children_.end(), child), children_.end());
}

void JWindow::SetCurrent() {
	if (IsCurrent()) 
		return;

	jApp->SetCurrent(this);
}

}	//namespace jfc
