#ifndef _JBUTTON_H_
#define _JBUTTON_H_

#include "JWindow.h"
#include <string>

namespace jfc {

class JWindow;

class JButton : public JWindow
{
public:
	JButton();
	JButton(SHORT x, SHORT y, SHORT width, SHORT height,
		const std::string &text = std::string(),
		JWindow *parent = jApp->Root());
	virtual ~JButton(void);
	virtual void Draw();

private:
	std::string text_;
};


}	//namespace jfc

#endif	//_JBUTTON_H_
